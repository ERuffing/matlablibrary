function [ perr ] = percentError( theoretical, experimental )
%percentError Calculates the percent error between given values
% FUNCTION: percentError
% AUTHOR:   Ethan Ruffing
% DATE:     January 14, 2015
% DESCRIPTION:  This function will calculate the percent error between a given
%     theoretical and experimental value.
% PARAMETERS:
%     theoretical: The theoretical value
%     experimental: The experimental value
% RETURNS:
%     perr: The percent error (in decimal form) between the given values
% EXAMPLES:
%     e = percentError(theor, dataAve);

if theoretical == 0
    error('percentError:divideByZero', ...
        'The theoretical value cannot be zero. Consider using absolute error.');
end

perr = abs(theoretical - experimental) / (theoretical);

end
