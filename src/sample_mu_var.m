function [ mean, var ] = sample_mu_var( data )
%sample_mu_var Calculates sample mean and sample variance of a vector of data
% FUNCTION: sample_mu_var
% AUTHOR:   Ethan Ruffing
% DATE:     February 4, 2015
% DESCRIPTION:  This function will calculate the sample mean and sample variance
%     of a vector of data.
% PARAMETERS:
%     data: The data to perform statistical calculations on
% RETURNS:
%     mean: The sample mean for the input data
%     var:  The sample variance for the input data

% Verify that the data passed is not empty or null
if isempty(data)
    error('sample_mu_var:emptyData', 'Data array is empty or null!');
end

% Calculate sample mean of data
mean = sum(data) ./ length(data);

% Zero-mean the data
diff = data - mean;

% Square the differences
diff = diff.^2;

% Find the variance
var = sum(diff) ./ (length(data) - 1);

end
