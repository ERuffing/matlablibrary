function [f] = wToF(w)
%wToF  Converts an angular velocity to a frequency.
% f = wToF(w) Converts w (in rad/s) to f (in Hertz)

f = w / (2 * pi);

end
