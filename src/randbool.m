function [r] = randbool (probability)
%randbool Generates a random boolean, governed by a specified probability
% Function: randbool
% AUTHOR:   Ethan Ruffing
% DATE:     January 21, 2015
% DESCRIPTION:  This function will generate a random boolean, with a specified
%     probability of being true.
% Parameters:
%     probability: The desired probability of receiving a true for the boolean
% Returns:
%     r: [boolean] The generated random boolean

r = rand < probability;

end
