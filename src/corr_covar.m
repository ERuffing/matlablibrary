function [ c, p ] = corr_covar( X, Y )
%corr_covar Calculates the correlation and covariance of X and Y
% FUNCTION: corr_covar
% AUTHOR:   Ethan Ruffing
% DATE:     March 18, 2015
% DESCRIPTION:  This function will calculate the covariance and correlation
%     between two variables X and Y.
% INPUTS:
%     X [vector]:   The first variable
%     Y [vector]:   The second variable
% RETURNS:
%     c [scalar]:   The covariance of X and Y
%     p [scalar]:   The correlation coefficient of X and Y

% Ensure that input arrays have same dimensions
if size(X) ~= size(Y)
    error('corr_covar:mismatch', 'Dimensions of X and Y must be identical.');
end

% Calculate mean, variance, and standard deviation of sample data
[meanX, varX] = sample_mu_var(X);
stdX = sqrt(varX);
[meanY, varY] = sample_mu_var(Y);
stdY = sqrt(varY);

% Create array of addends
s = X .* Y - (meanX * meanY);

% Calculate covariance
c = (1 / (length(s) - 1)) * sum(s);

% Calculate correlation
p = c / (stdX * stdY);

end
