function [ reject ] = ttest_onevar( X, mu_Ho, alpha )
%ttest_onevar Performs a two-sided Student's T-test on one random variable
% FUNCTION: ttest_onevar
% AUTHOR:   Ethan Ruffing
% DATE:     March 25, 2015
% DESCRIPTION:  This function will perform a two-sided Student's T-test for
%     one random variable on the data provided.
% PARAMETERS:
%     X:        The random variable data to test
%     mu_Ho:    The null hypothesis value for the mean of X
%     alpha:    The significance to test at
% RETURNS:
%     reject:   True if the null hypothesis should be rejected

[mu, var] = sample_mu_var(X);
stddev = sqrt(var);

L = 1 - alpha;

n = length(X);
df = n - 1;

dmu = (stddev / sqrt(n)) * tinv((L + 1) / 2, df);

reject = ~(((mu_Ho - dmu) <= mu) && ((mu_Ho + dmu) >= mu));

end
