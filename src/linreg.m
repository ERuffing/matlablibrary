function [ a, b ] = linreg( X, Y )
%linreg Calculates least-squares linear regression model parameters
% FUNCTION: linreg
% AUTHOR:   Ethan Ruffing
% DATE:     March 18, 2015
% DESCRIPTION:  This function will calculate the paramaters for a least-squares
%     model for a linear regression of the form Y = aX + b
% INPUTS:
%     X [vector]:   The independent variable data
%     Y [vector]:   The dependent variable data
% RETURNS:
%     a [scalar]:   The coefficient for X in the least-squares model
%     b [scalar]:   The y-intercept in the least-squares model

% Calculate correlation and covariance
[c, p] = corr_covar(X, Y);

% Calculate mean, variance, and standard deviation of sample data
[meanX, varX] = sample_mu_var(X);
stdX = sqrt(varX);
[meanY, varY] = sample_mu_var(Y);
stdY = sqrt(varY);

% Calculate a
a = p * stdY / stdX;

% Calculate b
b = meanY - a * meanX;

end
