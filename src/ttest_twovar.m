function [ reject, mu1, mu2, var1, var2, df, minT, maxT, t ] = ...
    ttest_twovar( X1, X2, alpha )
%ttest_onevar Performs a two-sided Student's T-test on two random variables
% FUNCTION: ttest_twovar
% AUTHOR:   Ethan Ruffing
% DATE:     April 8, 2015
% DESCRIPTION:  This function will perform a two-sided Student's T-test for
%     two random variables on the data provided. Assumes a null hypotheses of
%     mu_1 = mu_2
% PARAMETERS:
%     X1:       The first random variable data to test
%     X2:       The second random variable data to test
%     alpha:    The significance to test at
% RETURNS:
%     reject:   True if the null hypothesis should be rejected
%     mu1:      The average of the first set of data
%     mu2:      The average of the second set of data
%     var1:     The variance of the first set of data
%     var2:     The variance of the second set of data
%     df:       Degrees of freedom used for calculations
%     minT:     The minimum acceptable T value
%     maxT:     The maximum acceptable T value
%     t:        The calculated t value for the data provided

L = 1 - alpha;

[mu1, var1] = sample_mu_var(X1);
[mu2, var2] = sample_mu_var(X2);

n1 = length(X1);
n2 = length(X2);

df = (var1 / n1 + var2 / n2)^2 / (1 / (n1 - 1) * (var1 / n1)^2 + 1 / ...
    (n2 - 1) * (var2 / n2)^2);

t = (mu1 - mu2) / sqrt(var1 / n1 + var2 / n2);

T = tinv((L + 1) / 2, df);

minT = -T;

maxT = T;

reject = ~(minT <= t && t <= maxT);

end
