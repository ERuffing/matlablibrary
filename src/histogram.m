function [ counts ] = histogram( maximum, minimum, nBins, data )
%histogram Generates a histogram vector for a set of data
% FUNCTION: histogram
% AUTHOR:   Ethan Ruffing
% DATE:     January 28, 2015
% DESCRIPTION:  This function will produce a histogram vector for a set of data.
% PARAMETERS:
%     maximum: The upper end of the maximum bin
%     minimum: The lower end of the minimum bin
%     nBins: The number of bins to produce
%     data: The data to use for the histogram
% RETURNS:
%     counts: [vector] The (normalized) frequency counts for each bin

%% ERROR CHECKING

if ~(minimum < maximum)
    error('histogram:minmax', 'minimum must be less than maximum.');
end

if nBins < 1
    error('histogram:nbins', 'nBins must be at least 1.');
end

d = size(data);
if d(1,2) ~= 1;
    error('histogram:data', 'data must be passed as a vector.');
end

%% HISTOGRAM GENERATION

% The incremement between each bin
dbin = (maximum - minimum) / nBins;

% Create a vector of upper bin limits
bins = zeros(nBins, 1);
bins(nBins) = maximum;
for i = 1:(nBins - 1)
    bins(nBins - i) = bins(nBins - i + 1) - dbin;
end

% counts the data in each bin
counts = zeros(nBins, 1);
for i = 1:size(data)
    if data(i) <= minimum
        counts(1) = counts(1) + 1;
    elseif data(i) > maximum
        counts(nBins) = counts(nBins) + 1;
    else
        I = find(bins >= data(i), 1);
        counts(I) = counts(I) + 1;
    end
end

% Normalize data
counts = counts / sum(counts);
