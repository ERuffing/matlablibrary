function [] = plot_hist( minimum, maximum, nBins, data, plot_title)
%plot_hist Generates a histogram of data and plots it to figure and to file
% FUNCTION: plot_hist
% AUTHOR:   Ethan Ruffing
% DATE:     February 4, 2015
% DESCRIPTION:  This function acts as a wrapper function for `histogram`. It
%     will calculate and display a histogram for a set of data. It will use
%     specified values for the minimum and maximum bin values, and will use a
%     specified number of bins.
% PARAMETERS:
%     minimum:  The minimum bin level for the histogram
%     maximum:  The maximum bin level for the histogram
%     nBins:    The number of bins to include in the histogram
%     data:     The data to generate the histogram for
%     plot_title:   The title to place at the top of the plot
% REURNS:
%     none

% Calculate histogram data
histo = histogram(maximum, minimum, nBins, data);

% Set the locations for the histogram bars to be the midpoints of each bin
x = zeros(nBins, 1);
for i = 0:(nBins - 1);
    x(nBins - i) = maximum - ((maximum - minimum) / nBins) * (i + 0.5);
end

% Display histogram
bar(x, histo);
xlim([minimum, maximum]);
xlabel('Range');
ylabel('Density');
title(plot_title);

end
